import Horizon from '@horizon/client'

const hz = Horizon({ authType: 'unauthenticated' });
const questions = hz('questions');

export default {

	seed () {
		questions.store([
			{
				id: 1,
				quiz_id: 1,
				number: 1,
				title: 'Is this how you should answer the phone?',
				answer: 'incorrect',
				answers: {
					total: 0,
					correct: 0
				}
			},
			{
				id: 2,
				quiz_id: 1,
				number: 2,
				title: 'Is this how you should answer the phone?',
				answer: 'incorrect',
				answers: {
					total: 0,
					correct: 0
				}
			},
			{
				id: 3,
				quiz_id: 1,
				number: 3,
				title: 'Is this how you should answer the phone?',
				answer: 'incorrect',
				answers: {
					total: 0,
					correct: 0
				}	
			},
			{
				id: 4,
				quiz_id: 1,
				number: 4,
				title: 'Is this how you should answer the phone?',
				answer: 'correct',
				answers: {
					total: 0,
					correct: 0
				}	
			},
			{
				id: 5,
				quiz_id: 1,
				number: 5,
				title: 'Is this how you should answer the phone?',
				answer: 'incorrect',
				answers: {
					total: 0,
					correct: 0
				}	
			}
		])
	},

	reset () {
		this.fetch().subscribe(results => {
			questions.removeAll(results).subscribe(() => { this.seed() })
		})
	},

	find (params) {
		return questions.find(params).fetch()
	},

	fetch () {
		return questions.fetch()
	},

	changes () {
		return questions.watch()
	},

	submitAnswer (question_id, correct) {
		this.find({ id: question_id }).subscribe(result => {
			if(correct)
				++result.answers.correct

			++result.answers.total

			questions.upsert(result)
		})
	}

}