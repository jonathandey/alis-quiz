export default {
	data: {
		setup: false,
		participants: 0
	},

	finishSetup () {
		this.data.setup = true
	},

	addParticipant () {
		++this.data.participants
	}
}