import Horizon from '@horizon/client'

const hz = Horizon({ authType: 'unauthenticated' });
const quiz = hz('quiz');

export default {

	seed () {
		quiz.store({
			id: 1,
			currentQuestion: 1
		})
	},

	reset () {
		this.get().subscribe(result => {
			quiz.remove(result).subscribe(() => { this.seed() })
		})
	},

	get () {
		return quiz.find({ id: 1 }).fetch()
	},

	toBeginning () {
		this.get().subscribe(result => {
			result.currentQuestion = 1
			quiz.upsert(result)
		})
	},

	nextQuestion () {
		this.get().subscribe(result => {
			++result.currentQuestion
			quiz.upsert(result)

			console.log('now on question: ', result)
		})
	},

	changes () {
		return quiz.watch()
	}

}