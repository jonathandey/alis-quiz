import Vue from 'vue'
import VueRouter from 'vue-router'

import Quiz from './components/Quiz.vue'
import Host from './components/Host.vue'
import Client from './components/Client.vue'

Vue.use(VueRouter)

Vue.debug = true

let router = new VueRouter()

router.map({
    '/host': {
        component: Host
    },
    '/': {
        component: Client
    }
})

router.start(Quiz, '#app')


