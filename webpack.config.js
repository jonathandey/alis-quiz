module.exports = {
	entry: "./src/app.js",
	output: {
		path: "./dist/js",
		filename: "bundle.js"
	},
	module: {
		loaders: [
			{	
		        test: /\.js$/,
		        exclude: /node_modules/,
				loader: 'babel'
			},
			{
				test: /\.vue$/,
				exclude: /node_modules/,
				loader: 'vue'
			},
			{ test: /\.css$/, loader: "style!css" }
		]
	},
	babel: {
		presets: ['es2015'],
		plugins: ['transform-runtime']
	}
};